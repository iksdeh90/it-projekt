import os
import time

import utilities


def first_scan(ip):
    print(utilities.GREEN + ' starting nmap top 1000 ports on ' + ip + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('nmap -v -sV -sT --top-ports 1000 -oA ' + ip + '_tcp1000 ' + ip)


# TODO: vuln scan > script scan ?
def script_scan_tcp(ip, port_list):
    print(utilities.GREEN + ' starting nmap script scan on ' + ip + ' ' + utilities.END)
    ports = ','.join((map(str, port_list)))
    time.sleep(utilities.time_wait)
    os.system('nmap -v -sV -sT -A -p ' + ports + ' -oN ' + ip + '_script_scan_tcp.txt ' + ip)


def script_scan_udp(ip, port_list):
    print(utilities.GREEN + ' starting nmap udp scan on ' + ip + ' ' + utilities.END)
    ports = ','.join((map(str, port_list)))
    time.sleep(utilities.time_wait)
    os.system('nmap -v -sV -sU -A -p ' + ports + ' -oN ' + ip + '_script_scan_udp.txt ' + ip)


def udp_scan(ip):
    print(utilities.GREEN + ' starting nmap udp scan top 100 ports on ' + ip + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('nmap -v -sV -sU --top-ports 100 -oA ' + ip + '_udp100 ' + ip)


def vuln_scan_tcp(ip, port_list):
    """
    make sure you have installed vulnscan for nmap

    https://null-byte.wonderhowto.com/how-to/easily-detect-cves-with-nmap-scripts-0181925/

    cd /usr/share/nmap/scripts/
    git clone https://github.com/vulnersCom/nmap-vulners.git
    git clone https://github.com/scipag/vulscan.git

    :param ip:
    :param port_list:
    :return:
    """
    print(utilities.GREEN + ' starting nmap vuln scan for tcp ports on ' + ip + ' ' + utilities.END)
    ports = ','.join((map(str, port_list)))
    time.sleep(utilities.time_wait)
    os.system('nmap -Pn -oN ' + ip + '_vulnscan.txt --script nmap-vulners,vulscan --script-args '
                                     'vulscandb=scipvuldb.csv -sV -p ' + ports + ' ' + ip)


def vuln_scan_udp(ip, port_list):
    """
    make sure you have installed vulnscan for nmap

    https://null-byte.wonderhowto.com/how-to/easily-detect-cves-with-nmap-scripts-0181925/

    cd /usr/share/nmap/scripts/
    git clone https://github.com/vulnersCom/nmap-vulners.git
    git clone https://github.com/scipag/vulscan.git

    :param ip:
    :param port_list:
    :return:
    """
    print(utilities.GREEN + ' starting nmap vuln scan for udp ports on ' + ip + ' ' + utilities.END)
    ports = ','.join((map(str, port_list)))
    time.sleep(utilities.time_wait)
    os.system('nmap -Pn -oN ' + ip + '_vulnscan.txt --script nmap-vulners,vulscan --script-args '
                                     'vulscandb=scipvuldb.csv -sV -sU -p ' + ports + ' ' + ip)
