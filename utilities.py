
# color
RED = '\033[1;31;40m'
GREEN = '\033[1;32;40m'
YELLOW = '\033[1;33;40m'
BLUE = '\033[1;34;40m'

END = '\033[0m'

# time.sleep value
time_wait = 1.75
time_kill_process = 30
