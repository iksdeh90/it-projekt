# IT-Projekt 

## Tested with:
Distributor ID:	Kali  
Description:	Kali GNU/Linux Rolling  
Release:        2019.4  
Codename:       kali-rolling  

## Require:
Python 3.5

Before using, install [vulnscan for nmap](https://null-byte.wonderhowto.com/how-to/easily-detect-cves-with-nmap-scripts-0181925/):  
* cd /usr/share/nmap/scripts/  
* git clone https://github.com/vulnersCom/nmap-vulners.git  
* git clone https://github.com/scipag/vulscan.git  

## Usage:
git clone https://gitlab.com/iksdeh90/it-projekt

chmod +x main.py

main.py 127.0.0.1