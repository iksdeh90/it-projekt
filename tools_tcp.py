import os
import time

import utilities

common_wordlist = '/usr/share/wordlists/dirb/common.txt'


def start_enum4linux(ip):
    print(utilities.GREEN + ' starting enum4linux on ' + ip + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('enum4linux -a ' + ip + ' > ' + ip + '_enum4linux_tcp.txt')
    print(utilities.GREEN + ' finished enum4linux on ' + ip + ' ' + utilities.END)


def start_rpcbind(ip):
    print(utilities.GREEN + ' starting rpcinfo on ' + ip + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('rpcinfo ' + ip + ' > ' + ip + '_rpcinfo.txt')
    print(utilities.GREEN + ' finished rpcinfo on ' + ip + ' ' + utilities.END)


def start_wfuzz(ip, port):
    print(utilities.GREEN + ' starting wfuzz on ' + ip + ':' + port + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('wfuzz -w ' + common_wordlist + ' --hc 400,401,402,403,404 ' + ip + ':' + port + '/FUZZ > ' + ip +
              '_wfuzz_commom.txt')
    print(utilities.GREEN + ' finished wfuzz on ' + ip + ':' + port + ' ' + utilities.END)


def start_sslscan4https(ip, port):
    print(utilities.GREEN + ' starting sslscan ' + ip + ':' + port + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('sslscan ' + ip + ':' + port + ' > ' + ip + '_sslscan_from_https.txt')
    print(utilities.GREEN + ' finished sslscan ' + ip + ':' + port + ' ' + utilities.END)


def start_sslscan4smtps(ip, port):
    print(utilities.GREEN + ' starting sslscan ' + ip + ':' + port + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('sslscan --starttls ' + ip + ':' + port + ' > ' + ip + '_sslscan_from_smtps.txt')
    print(utilities.GREEN + ' finished sslscan ' + ip + ':' + port + ' ' + utilities.END)


def start_nikto(ip, port):
    print(utilities.GREEN + ' starting nikto ' + ip + ':' + port + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('nikto -h ' + ip + ':' + port + ' > ' + ip + '_nikto.txt')
    print(utilities.GREEN + ' finished nikto ' + ip + ':' + port + ' ' + utilities.END)


def start_openssl(ip, port):
    print(utilities.GREEN + ' starting openssl ' + ip + ':' + port + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('openssl s_client -connect ' + ip + ':' + port + ' > ' + ip + '_openssl.txt')
    print(utilities.GREEN + ' finished openssl ' + ip + ':' + port + ' ' + utilities.END)
