import port_scan
import xml_parser
import tcp_threads
import udp_threads
import terminate_threads
import utilities


def start_scanning(ip):
    # Scan top 1000 ports first
    port_scan.first_scan(ip)
    # Parse the XML form the first scan
    xml_parser.parse_tcp_xml(ip)
    scan_tcp_services(ip)

    while True:
        print(utilities.YELLOW + 'UDP scan (y/n)? Could take much time!' + utilities.END)
        decision = str(input())
        if decision == 'y':
            port_scan.udp_scan(ip)
            xml_parser.parse_udp_xml(ip)
            scan_udp_services(ip)
            break
        elif decision == 'n':
            print(utilities.BLUE + 'All nmap-scans are complete.' + utilities.END)
            break
        elif decision is not ('y' or 'n'):
            print(utilities.YELLOW + 'Only use y (yes) or n (no)!' + utilities.END)
            continue

    # will not execute in the final project, watch documentation
    # terminate_threads.check_threads()


def remove_tcp_items(index):
    xml_parser.tcp_services.pop(index)
    xml_parser.tcp_ports.pop(index)


def remove_udp_items(index):
    xml_parser.udp_services.pop(index)
    xml_parser.udp_ports.pop(index)


def scan_tcp_services(ip):
    """
    add here plugins like this

    if <service name> in xml_parser.tcp_services:
        index = xml_parser.all_tcp_services.index(<service name>)
        port = xml_parser.all_ports[index]

        <your file>.<your function(<your params>)>

        remove_items(index)

    If your scan is final, start it in a new thread (faster)

    delete the service and port from the list
    reason: maybe http on 80 and another on 8080.

    xml_parser.tcp_services will take the services from the tcp top 1000 port scan
    add an parser in xml_parser.py for your own xml if needed

    :param ip:
    :return:
    """

    loop_len = len(xml_parser.tcp_ports)

    # If some open ports found, start an script scan on these ports
    if xml_parser.tcp_services:
        while True:
            print(utilities.YELLOW + 'Open TCP Ports found. Start script-scan? (y/n)' + utilities.END)
            decision = str(input())
            if decision == 'y':
                port_scan.vuln_scan_tcp(ip, xml_parser.tcp_ports)
                port_scan.script_scan_tcp(ip, xml_parser.tcp_ports)
                break
            elif decision == 'n':
                print(utilities.BLUE + 'Starting Tools for open ports.' + utilities.END)
                break
            elif decision is not ('y' or 'n'):
                print(utilities.YELLOW + 'Only use y (yes) or n (no)!' + utilities.END)
                continue

    if loop_len > 0:
        for i in range(loop_len):   # if no ports given, not needed
            if 'smtps' in xml_parser.tcp_services:
                index = xml_parser.tcp_services.index('smtps')
                port = xml_parser.tcp_ports[index]

                tcp_threads.scan_ssl4smtps(ip, port)

                remove_tcp_items(index)

            if ('netbios-ssn' or 'microsoft-ds') in xml_parser.tcp_services:
                tcp_threads.scan_netbios(ip)

                # One scan with enum4linux is enought
                while ('netbios-ssn' or 'microsoft-ds') in xml_parser.tcp_services:
                    if 'netbios-ssn' in xml_parser.tcp_services:
                        index = xml_parser.tcp_services.index('netbios-ssn')
                        remove_tcp_items(index)

                    if 'microsoft-ds'in xml_parser.tcp_services:
                        index = xml_parser.tcp_services.index('microsoft-ds')
                        remove_tcp_items(index)

            if 'rpcbind' in xml_parser.tcp_services:
                tcp_threads.scan_rpcbind(ip)

                # One scan with rpcinfo is enought
                while 'rpcbind' in xml_parser.tcp_services:
                    remove_tcp_items(xml_parser.tcp_services.index('rpcbind'))

            # caldav is an service from 'python -m SimpleHTTPServer', its just for testing
            if 'caldav' in xml_parser.tcp_services:
                index = xml_parser.tcp_services.index('caldav')
                port = xml_parser.tcp_ports[index]

                tcp_threads.scan_caldav(ip, port)

                remove_tcp_items(index)

            if 'http' in xml_parser.tcp_services:
                index = xml_parser.tcp_services.index('http')
                port = xml_parser.tcp_ports[index]

                tcp_threads.scan_http(ip, port)
                tcp_threads.scan_nikto(ip, port)

                remove_tcp_items(index)

            if 'https' in xml_parser.tcp_services:
                index = xml_parser.tcp_services.index('https')
                port = xml_parser.tcp_ports[index]

                tcp_threads.scan_https(ip, port)
                tcp_threads.scan_nikto(ip, port)
                tcp_threads.scan_ssl4https(ip, port)
                tcp_threads.scan_openssl(ip, port)

                remove_tcp_items(index)


def scan_udp_services(ip):
    """
        add here plugins like this

        if <service name> in xml_parser.tcp_services:
            index = xml_parser.udp_services.index(<service name>)
            port = xml_parser.udp_ports[index]

            <your file>.<your function(<your params>)>

            remove_items(index)

        If your scan is final, start it in a new thread (faster)

        delete the service and port from the list
        reason: maybe http on 80 and another on 8080.

        xml_parser.udp_services will take the services from the udp top 100 port scan
        add an parser in xml_parser.py for your own xml if needed

        :param ip:
        :return:
    """

    loop_len = len(xml_parser.udp_ports)

    if xml_parser.udp_services:
        while True:
            print(utilities.YELLOW + 'Open UDP Ports found. Start script-scan? (y/n)' + utilities.END)
            print(utilities.YELLOW + 'UDP-Scans can take much time!' + utilities.END)
            decision = str(input())
            if decision == 'y':
                port_scan.vuln_scan_udp(ip, xml_parser.udp_ports)
                port_scan.script_scan_udp(ip, xml_parser.udp_ports)
                break
            elif decision == 'n':
                print(utilities.BLUE + 'Starting Tools for open ports.' + utilities.END)
                break
            elif decision is not ('y' or 'n'):
                print(utilities.YELLOW + 'Only use y (yes) or n (no)!' + utilities.END)
                continue

    if loop_len > 0:
        for i in range(loop_len):  # if no ports given, not needed
            if ('netbios-ssn' or 'microsoft-ds') in xml_parser.udp_services:
                index = xml_parser.udp_services.index('netbios-ssn')

                udp_threads.scan_netbios(ip)

                remove_udp_items(index)

            if 'snmp' in xml_parser.udp_services:
                index = xml_parser.udp_services.index('snmp')
                port = xml_parser.udp_ports[index]

                udp_threads.scan_snmp(ip, port)

                remove_udp_items(index)
