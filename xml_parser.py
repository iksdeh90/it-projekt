import xml.etree.ElementTree as ET


tcp_ports = []
tcp_services = []

udp_ports = []
udp_services = []


def parse_tcp_xml(name):
    """
    this function will parse an xml file from nmap

    create another tree with your xml file from nmap if you need it

    :param name:
    :return:
    """
    tree_tcp = ET.parse(name + '_tcp1000.xml')
    root = tree_tcp.getroot()

    for port in root.iter('port'):
        port_id = port.get('portid')
        tcp_ports.append(port_id)

    for service in root.iter('service'):
        service_name = service.get('name')
        tcp_services.append(service_name)


def parse_udp_xml(name):
    """
    this function will parse an xml file from nmap

    create another tree with your xml file from nmap if you need it

    :param name:
    :return:
    """
    tree_udp = ET.parse(name + '_udp100.xml')
    root = tree_udp.getroot()

    for port in root.iter('port'):
        port_id = port.get('portid')
        udp_ports.append(port_id)

    for service in root.iter('service'):
        service_name = service.get('name')
        udp_services.append(service_name)
