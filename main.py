#!/usr/bin/env python3

import sys
import ipaddress

import scanner

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('No IP given!')
        sys.exit(0)
    else:
        try:
            ip = str(ipaddress.IPv4Address(sys.argv[1]))
        except ValueError:
            print('Wrong IP-Format!')
            sys.exit(0)

    scanner.start_scanning(ip)
