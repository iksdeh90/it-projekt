import os
import time

import utilities


def start_enum4linux(ip):
    print(utilities.GREEN + ' starting enum4linux on ' + ip + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('enum4linux -a ' + ip + ' > ' + ip + '_enum4linux_udp.txt')
    print(utilities.GREEN + ' finished enum4linux on ' + ip + ' ' + utilities.END)


def start_snmpwalk(ip, port):
    print(utilities.GREEN + ' starting snmpwalk ' + ip + ':' + port + ' ' + utilities.END)
    time.sleep(utilities.time_wait)
    os.system('snmpwalk -c public -v1 ' + ip + ':' + port + ' > ' + ip + '_snmpwalk.txt')
    print(utilities.GREEN + ' finished snmpwalk ' + ip + ':' + port + ' ' + utilities.END)
