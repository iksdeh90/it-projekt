from multiprocessing import Process

import tools_udp

plist_udp = []


def scan_netbios(ip):
    p_netbios_scan = Process(target=tools_udp.start_enum4linux, args=[ip])
    plist_udp.append(p_netbios_scan)
    p_netbios_scan.start()
    plist_udp.remove(p_netbios_scan)


def scan_snmp(ip, port):
    p_snmp = Process(target=tools_udp.start_snmpwalk, args=[ip, port])
    plist_udp.append(p_snmp)
    p_snmp.start()
    plist_udp.remove(p_snmp)
