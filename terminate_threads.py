import time
import sys
from multiprocessing import Process

import tcp_threads
import udp_threads
import utilities


def check_threads():
    time.sleep(utilities.time_wait)
    print(utilities.BLUE + ' looking for undergoing processes ~ ' + str(utilities.time_kill_process) + ' seconds '
          + utilities.END)

    time.sleep(utilities.time_kill_process)

    loop_len = len(tcp_threads.plist_tcp) + len(udp_threads.plist_udp)

    if loop_len > 0:
        for process in range(loop_len):
            try:
                killme = tcp_threads.plist_tcp[process]
                if killme.is_alive():
                    killme.terminate()
                    killme.join()

                    print(utilities.BLUE + ' terminate undergoing process ' + utilities.END)

            except ValueError:
                print(utilities.BLUE + ' all scans complete (try) ' + utilities.END)
                sys.exit(0)

        print(utilities.BLUE + ' all scans complete ' + utilities.END)

    else:
        print(utilities.BLUE + ' all scans complete ' + utilities.END)
        sys.exit(0)
