from multiprocessing import Process

import tools_tcp

plist_tcp = []


def scan_netbios(ip):
    p_netbios_scan = Process(target=tools_tcp.start_enum4linux, args=[ip])
    plist_tcp.append(p_netbios_scan)
    p_netbios_scan.start()


def scan_rpcbind(ip):
    p_rpcbind_scan = Process(target=tools_tcp.start_rpcbind, args=[ip])
    plist_tcp.append(p_rpcbind_scan)
    p_rpcbind_scan.start()


def scan_caldav(ip, port):
    p_caldav_scan = Process(target=tools_tcp.start_wfuzz, args=[ip, port])
    plist_tcp.append(p_caldav_scan)
    p_caldav_scan.start()


def scan_http(ip, port):
    p_http_scan = Process(target=tools_tcp.start_wfuzz, args=[ip, port])
    plist_tcp.append(p_http_scan)
    p_http_scan.start()


def scan_nikto(ip, port):
    p_nikto_scan = Process(target=tools_tcp.start_nikto, args=[ip, port])
    plist_tcp.append(p_nikto_scan)
    p_nikto_scan.start()


def scan_https(ip, port):
    p_https_scan = Process(target=tools_tcp.start_wfuzz, args=[ip, port])
    plist_tcp.append(p_https_scan)
    p_https_scan.start()


def scan_ssl4https(ip, port):
    p_sslscan4https = Process(target=tools_tcp.start_sslscan4https, args=[ip, port])
    plist_tcp.append(p_sslscan4https)
    p_sslscan4https.start()


def scan_ssl4smtps(ip, port):
    p_sslscan4smtps = Process(target=tools_tcp.start_sslscan4smtps, args=[ip, port])
    plist_tcp.append(p_sslscan4smtps)
    p_sslscan4smtps.start()


def scan_openssl(ip, port):
    p_openssl = Process(target=tools_tcp.start_openssl, args=[ip, port])
    plist_tcp.append(p_openssl)
    p_openssl.start()
